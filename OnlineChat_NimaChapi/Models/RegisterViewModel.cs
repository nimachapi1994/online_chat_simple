﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineChat_NimaChapi.Models
{
    //public class User_Role 
    //{
    //    [Key]
    //    public int Id { get; set; }
    //    public string RoleName { get; set; }
    //    public string User_Index { get; set; }
    //    public RegisterViewModel User { get; set; }
    //}
    public class RegisterViewModel
    {
        [Key]
        public int Id { get; set; }
        public string User_Index { get; set; }
        [Display(Name = "شماره موبایل")]
        [Required(ErrorMessage = "{0} نمیتواند خالی باشد", AllowEmptyStrings = false)]
        [MaxLength(11, ErrorMessage = "{0} نمی تواند بیش از 11 کارکتر باشد")]
        [MinLength(11, ErrorMessage = "{0} نمی تواند کمتر از 11 کارکتر باشد")]
        public string Phone { get; set; }
        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "{0} نمیتواند خالی باشد", AllowEmptyStrings = false)]

        public string Password { get; set; }
        [Display(Name = "نام")]
        [Required(ErrorMessage = "{0} نمیتواند خالی باشد", AllowEmptyStrings = false)]


        public string Name { get; set; }

        
        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "{0} نمیتواند خالی باشد", AllowEmptyStrings = false)]


        public string Fname { get; set; }
    }
}
