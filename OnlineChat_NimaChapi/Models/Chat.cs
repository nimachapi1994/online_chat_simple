﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineChat_NimaChapi.Models
{
    public class Chat
    {
        public int Id { get; set; }
        public int User_Id_Sended { get; set; }
        public int User_Id_Recived { get; set; }
        public string Message { get; set; }
    }
}
