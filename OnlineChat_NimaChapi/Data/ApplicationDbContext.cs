﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;

using OnlineChat_NimaChapi.Models;

namespace OnlineChat_NimaChapi.Data
{
    public class ApplicationDbContext : DbContext
    {
       
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer("data source=.;initial catalog=NimaEshop;integrated security=True;MultipleActiveResultSets=True");
          //  builder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\NimaChapi\Documents\EshopDb.mdf;Integrated Security=True;");
        }

        public virtual DbSet<RegisterViewModel> Tbl_User { get; set; }
        public virtual DbSet<Chat> Tbl_Chat { get; set; }
       




    }
}
