﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using OnlineChat_NimaChapi.Controllers;
using OnlineChat_NimaChapi.Data;
using OnlineChat_NimaChapi.Models;
using OnlineChat_NimaChapi.Uility;

namespace Onlie_ShopMrBadr.Areas.User.Controllers
{
    
    public class AccountController : Controller
    {
        ApplicationDbContext db = null;
        public AccountController()
        {
            db = new ApplicationDbContext();
        }
        public IActionResult LoginConfirm(LoginViewModel model)
        {
            string HashPass = BuildHashStrings.HashString(model.Password);
            var QueryGetLogin = db.Tbl_User.Where
                (x => x.Phone == model.Phone && x.Password == HashPass)
                .FirstOrDefault();


            object CookieLoginSuccessfully = new
            {
                phone = QueryGetLogin.Phone,
                password = QueryGetLogin.Password,
                Name = QueryGetLogin.Name,
                Fname = QueryGetLogin.Fname
            };
            var Login_Data = Newtonsoft.Json.JsonConvert.SerializeObject(CookieLoginSuccessfully);

            //Insert json objects to session and cookie
            HttpContext.Session.SetString("SessionLoginSuccessfully", Login_Data.ToString());
            CookieOptions cookie = new CookieOptions { Expires = DateTime.Now.AddDays(7) };
            Response.Cookies.Append("CookieLoginSuccessfully", Login_Data.ToString(), cookie);

            return RedirectToAction("Index", "Home");






        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("SessionLoginSuccessfully");
            Response.Cookies.Delete("CookieLoginSuccessfully");
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Register(string ok = null)
        {
            if (ok != null)
            {
                TempData["Ok"] = ok as string;
                return View();
            }
            else
            {

                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RegisterConfirm(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                var phoneNumberIsExists = register.Phone;
                var queryFindRepeatedPhoneNumber =
                    db.Tbl_User.Where(x => x.Phone == phoneNumberIsExists).Any();
                if (queryFindRepeatedPhoneNumber == false)
                {
                 
                    string User_Index = BuildHashStrings.HashString(register.Password);
                    RegisterViewModel registerViewModel =
                   new RegisterViewModel()
                   {

                       User_Index = User_Index,
                       Phone = register.Phone,
                       Password = BuildHashStrings.HashString(register.Password),
                       Name = register.Name,
                       Fname = register.Fname

                   };
                    
                    db.Tbl_User.Add(registerViewModel);
                  

                    db.SaveChanges();
                    string ok = "ثبت نام با موفقیت انجام شد";
                    return RedirectToAction(nameof(Register), new { ok = ok });
                }
                else
                {

                    string ok = "شماره تلفن وارد شده تکراری می باشد";
                    return RedirectToAction(nameof(Register), new { ok = ok });
                }

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}