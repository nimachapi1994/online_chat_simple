﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineChat_NimaChapi.Data;
using OnlineChat_NimaChapi.Models;

namespace OnlineChat_NimaChapi.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db;
        public HomeController()
        {
            db = new ApplicationDbContext();
        }

        public IActionResult Index()
        {
           
            RegisterViewModel
                getLoginDataFromSessionOrCookieBy_Json = null;


            //   Response.Cookies.Delete("CookieLoginSuccessfully");
            string session = HttpContext.Session.GetString("SessionLoginSuccessfully");
            var Cookie = Request.Cookies["CookieLoginSuccessfully"];
            if (session == null && Cookie == null)
            {
                return View();
            }
            else
            {
                if (session != null)
                {
                    //Get Login Data From Session
                    getLoginDataFromSessionOrCookieBy_Json =
                        JsonConvert.DeserializeObject<RegisterViewModel>(session);
                }
            
            }
            TempData["Logged"] = getLoginDataFromSessionOrCookieBy_Json;
            return View();

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
