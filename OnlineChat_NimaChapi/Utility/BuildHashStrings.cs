﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineChat_NimaChapi.Uility
{
    public class BuildHashStrings
    {
        public static string HashString(string x)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] xinput = System.Text.Encoding.ASCII.GetBytes(x);
                byte[] hash = md5.ComputeHash(xinput);
                StringBuilder str = new StringBuilder();

                for (int i = 0; i < hash.Length; i++)
                {
                    str.Append(hash[i].ToString());
                }
                return str.ToString();
            }
        }
    }
}
